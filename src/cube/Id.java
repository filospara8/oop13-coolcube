package cube;

//contiene tutti gli id delle entità presenti nel gioco
public enum Id {
	player, wall, mushroom, goomba, coin, flag;
}
